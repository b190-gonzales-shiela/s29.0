/*

2. Find users with letter s in their first name or t in their last name.
- Use the $or operator.
- Show only the firstName and lastName fields and hide the _id field.
3. Find users who are from the HR department and their age is greater then or equal to 70.
- Use the $and operator
4. Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators
5. Create a git repository named S29.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle


*/


/*2. Find users with letter s in their first name or t in their last name.
- Use the $or operator.
- Show only the firstName and lastName fields and hide the _id field.*/




db.users.find(
    {
        $or:[{firstName: {$regex:"s",$options:"$i"}},{lastName: {$regex:"t",$options:"$i"}}]
    },

    {
        _id:0,
        firstName:1,
        lastName:1
    }
);


//  3. Find users who are from the HR department and their age is greater then or equal to 70.
// - Use the $and operator



db.users.find({$and: [{age:{$gte:70}},{department:"HR"}]})


/*4. Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators*/


db.users.find({$and: [{age:{$lte:30}},{firstname: {$regex:"e", $options: "E"}}]})


